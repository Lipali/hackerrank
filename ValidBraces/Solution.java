public class Solution {
    public static void main(String[] args) {
        String braces = "([{}])"  ;
        boolean result =validator(braces);
        System.out.println(result);

    }
    static boolean validator(String text){
        boolean flag = true;
        int counterA =0;//{
        int counterB =0;//}
        int counterC =0;//(
        int counterD =0;//)
        int counterE =0;//[
        int counterF =0;//]
        if (text.length()%2!=0){
            flag = false;
        }
        String subTextA = text.substring(0,text.length()/2-1);
        String subTextB = text.substring(text.length()/2,text.length()-1);

        char [] charArrayA =text.toCharArray();
        char [] charArrayB =text.toCharArray();
        for (int i=0;i<charArrayA.length;i++){
            if (charArrayA[i]=='{'){
                counterA++;
            }
            if (charArrayA[i]=='('){
                counterC++;
            }
            if (charArrayA[i]=='['){
                counterE++;
            }
        }
        for (int i=0;i<charArrayB.length;i++){
            if (charArrayB[i]=='}'){
                counterB++;
            }
            if (charArrayB[i]==')'){
                counterD++;
            }
            if (charArrayB[i]==']'){
                counterF++;
            }
        }
        if (((counterA==counterB)&&(counterC==counterD))&&(counterE==counterF))
            flag= true;
        else
            flag=false;

        return flag;
    }
}
