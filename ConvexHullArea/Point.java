public class Point implements Comparable<Point>{
    double x;
    double y;


    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {return x;}

    public double getY() {
        return y;
    }
    public double square (){
        double temp =Math.sqrt(x*x+y*y);
        temp *=10;
        temp = Math.round(temp);
        temp /= 10;
        return temp;
    }


    @Override
    public int compareTo(Point o) {
        return (int) (this.square()- o.square())*-1;
    }
}
