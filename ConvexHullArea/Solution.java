import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {

        List<Point> pointsList = new LinkedList<>();
        pointsList.add(new Point(3,4));
        pointsList.add(new Point(4,5));
        pointsList.add(new Point(6,4));
        pointsList.add(new Point(7,4));
        pointsList.add(new Point(8,1));
        pointsList.add(new Point(4,4));
        pointsList.add(new Point(8,4));
        pointsList.add(new Point(2,4));

        for (Point showTest :pointsList){
            System.out.println(showTest.square());
        }
        System.out.println("           ");
        Collections.sort(pointsList);
        for (Point showTest :pointsList){
            System.out.println(showTest.square());
        }

    }
}
