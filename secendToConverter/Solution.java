package humanreadabledurationformat;

public class Solution {
    public static void main(String[] args) {

        int secends = 36200000;
        System.out.println(humanReadableDurationFormat(secends));

    }
    public static String humanReadableDurationFormat(int x){
        String result = "";
        StringBuilder sB = new StringBuilder(result);
        int tempSecends;
        int tempMinutes;
        int tempHour;
        int tempMonth;
        int tempYear;

        int temp = x/(3600*24*30*365);
        System.out.println(temp);
        if (temp>0){
            sB.append(temp + " years : ");
            x= x-60*60*24*30*365*temp;
        }
        temp = x/(60*60*24*30);
        if(temp>0){
            if (temp==1) {
                sB.append(temp + " month ");
            }
            else{
                sB.append(temp + " months ");
            }
            x=x-60*60*24*30*temp;
        }
        temp = x/(60*60*24);
        if(temp>0){
            if (temp==1) {
                sB.append(temp + " day ");
            }
            else{
                sB.append(temp + " days ");
            }
            x=x-60*60*24*temp;
        }
        temp = x/(60*60);

        if(temp>0){
            if (temp==1) {
                sB.append(temp + " hour ");
            }
            else{
                sB.append(temp + " hours ");
            }
            x=x-60*60*temp;
        }
        temp = x/60;
        if(temp>0){
            if (temp==0) {
                sB.append(temp + " minute ");
            }
            else{
                sB.append(temp + " minutes ");
            }
            x=x-60*temp;
        }

        if (x==1){
            sB.append(x + " secend")  ;
        }
        else {
            sB.append(x + " secends");
        }
        result = sB.toString();

        return result;
    }
}
