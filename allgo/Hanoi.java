import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class Hanoi {
    public static void main(String[] args)throws NoSuchElementException {
        LinkedList<Integer> listA = new LinkedList<>();
        LinkedList<Integer> listB = new LinkedList<Integer>();
        LinkedList<Integer> listC = new LinkedList<Integer>();

        listA.add(7);
        listA.add(6);
        listA.add(5);
        listA.add(4);
        listA.add(3);
        listA.add(2);
        listA.add(1);

        int counter = listA.size();
        int sizeListA =0;
        while (counter != listC.size()) {
            sizeListA = listA.size();
            if (counter % 2 == 0) {
                move(listA, listB);
                move(listA, listC);
                move(listB, listC);
            } else {
                move(listA, listC);
                move(listA, listB);
                move(listC, listB);
            }

        }
        for (Integer numbers : listC) {
            System.out.println(numbers);
        }
    }
    public static void move(List<Integer> listC, List<Integer> listD) {
        int tempSiazeC= listC.size();
        int tempSiazeD= listD.size();
        if (tempSiazeC==0){
            Integer temp = listD.get(tempSiazeD-1);
            listD.remove(tempSiazeD-1);
            listC.add(temp);
        }
        if (listD.size()==0){
            Integer temp = listC.get(listC.size()-1);
            listC.remove(listC.size()-1);
            listD.add(temp);
        }
        else{
            if (listC.get(listC.size()-1)>listD.get(listD.size()-1)){
                Integer temp = listC.get(listC.size()-1);
                listC.remove(listC.size()-1);
                listD.add(temp);
            }
            else if (listC.get(listC.size()-1)<listD.get(listD.size()-1)){
                Integer temp = listD.get(listD.size()-1);
                listD.remove(listD.size()-1);
                listC.add(temp);
            }
        }
    }
}
