public class Solution {
    public static void main(String[] args) {
        int [][] first = {
                { 5, 3, 4, 6, 7, 8, 9, 1,2},
                {6, 7, 2, 1, 9, 5, 3, 4, 8},
                {1, 9, 8, 3, 4, 2, 5, 6, 7},
                { 8, 5, 9, 7, 6, 1, 4, 2,3},
                {4, 2, 6, 8, 5, 3, 7, 9, 1},
                {7, 1, 3, 9, 2, 4, 8, 5, 6},
                {9, 6, 1, 5, 3, 7, 2, 8, 4},
                {2, 8, 7, 4, 1, 9, 6, 3, 5},
                {3, 4, 5, 2, 8, 6, 1, 7, 9}
        };
        int [][] secend ={
                {5, 3, 4, 6, 7, 8, 9, 1, 2},
                {6, 7, 2, 1, 9, 0, 3, 4, 8},
                {1, 0, 0, 3, 4, 2, 5, 6, 0},
                {8, 5, 9, 7, 6, 1, 0, 2, 0},
                {4, 2, 6, 8, 5, 3, 7, 9, 1},
                {7, 1, 3, 9, 2, 4, 8, 5, 6},
                {9, 0, 1, 5, 3, 7, 2, 1, 4},
                {2, 8, 7, 4, 1, 9, 6, 3, 5},
                {3, 0, 0, 4, 8, 1, 1, 7, 9}
        };
        boolean result;
        result = sudokuValidator(first);
        System.out.println(result);

    }
    static boolean sudokuValidator(int[][] tab){
        boolean flagLine = true;
        boolean flagNumber =true;
        int [] temp = new int[9];
        for (int i=0;i<9;i++){
            for (int j=0;j<9;j++){
                temp[j]=tab[i][j];
                flagNumber =numberValidator(temp[j]);
                if (flagNumber != true) {
                    flagNumber = false;
                    break;
                }
            }
            flagLine = lineValidator(temp);
            if (flagLine!=true){
                break;
            }
        }
        if ((flagLine!=false)&&(flagNumber!=false))
        return true;
        else
            return false;
    }
    static boolean numberValidator(int number){
        boolean flag = true;
        if ((number<10)&&(number>0))
            flag= true;
        else {
            flag= false;
        }
        return flag;
    }
    static boolean lineValidator(int[] tab){
        boolean flag=true;
        int tempTab [] = {1,0,0,0,0,0,0,0,0,0};

        for (int i=0;i< tab.length;i++ ) {
            tempTab[tab[i]]++;
        }
        for (int i=0;i< tempTab.length;i++){
            if (tempTab[i]==1){
                flag = true;
            }
            else{
                flag = false;
                break;
            }
        }
        return flag;
    }

}
