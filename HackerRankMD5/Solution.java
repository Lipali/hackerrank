import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            String s  = scanner.nextLine();
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
                md.update(s.getBytes());
                byte[] digest = md.digest();
                for (byte b : digest) {
                    System.out.printf("%02x", b);
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }
}