import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args)throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    int n;
    int p;
    long result;
    Scanner scanner = new Scanner(System.in);
    String line = scanner.nextLine();
    String [] values = line.split(" ");
    n = Integer.valueOf(values[0]);
    p = Integer.valueOf(values[1]);
        if(n==0 && p==0){
            throw new Exception("n and p should not be zero.");
        }
        if(n<0 || p<0){
            throw new Exception("n or p should not be negative.");
        }
        result=(long)Math.pow(n,p);
        System.out.println(result);
    }
}
