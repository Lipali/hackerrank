import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(tribo(3));
        System.out.println(triboBetter(n));

    }
    static int tribo (int n){
        if (n==0) {
            return 0;
        }
        if (n==1){
            return 1;
        }
        if (n==2){
            return 2;
        }
        return tribo(n-1)+tribo(n-2)+tribo(n-3);
    }
    static Map<Integer,BigInteger> results = new HashMap<>();
    static BigInteger triboBetter (int n){
        if (n==0){
            return BigInteger.ZERO;
        }
        if (n==1){
            return BigInteger.ONE;
        }
        if (n==2){
            return BigInteger.TWO;
        }
        if (results.containsKey(n)){
            return results.get(n);
        }
        BigInteger result = (triboBetter(n-1).add(triboBetter(n-2)).add(triboBetter(n-3)));
        results.put(n,result);
        return result;
    }


}
