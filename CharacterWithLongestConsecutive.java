public class CharacterWithLongestConsecutive {

    public static void main(String[] args) {

        String s = "aabbbccaa";
        solution(s);

    }
    public static void solution (String s){
        char [] tabChars = s.toCharArray();
        char temp =' ';
        int counter = 0;
        int tempCounter =1;

        for (int i=0;i< tabChars.length-1;i++){
            if (tabChars[i]==tabChars[i+1]){
                tempCounter++;
            }
            else{
                tempCounter=1;
            }
            if (tempCounter>counter){
                counter=tempCounter;
                temp=tabChars[i];
            }

        }
        System.out.println(temp+"  "+counter);
    }
}
