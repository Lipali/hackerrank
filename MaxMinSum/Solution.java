import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

import static java.util.stream.Collectors.*;

class Result {

    /*
     * Complete the 'miniMaxSum' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static void miniMaxSum(List<Integer> arr) {
        // Write your code here
        int [] numbers = new int[arr.size()];
        for (int i=0;i<arr.size();i++){
         numbers[i] = arr.get(i).intValue();
        }
        long sum =0;
        for (int i=0;i<numbers.length;i++){
            sum =  sum +(long) numbers[i] ;
        }

        int minNumber = numbers[0];
        for (int i=1;i<numbers.length;i++){
            if(minNumber>=numbers[i]){
                minNumber = numbers[i];
            }
        }
        int maxNumber = numbers[0];
        for (int i=0; i<numbers.length;i++){
            if (maxNumber<=numbers[i]){
                maxNumber=numbers[i];
            }
        }
        long sumMin = sum - (long)maxNumber;
        long sumMax = sum -(long) minNumber;

        System.out.println( sumMin+" "+ sumMax );

    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        List<Integer> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        Result.miniMaxSum(arr);

        bufferedReader.close();
    }
}

