import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class Main{
    private  static Sheets sheetsService;
    private static String APLICATION_NAME = "Google Sheets Experience";
    private static  String SPREAD_SHEET_ID="1QZYn0lDMx9LnmvhuLRasu1qjvFaFXLKwKTLMJOKRBOc";

    public static Credential authorize()throws IOException, GeneralSecurityException{
        InputStream in = Main.class.getResourceAsStream("/credentials.json");
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(
                JacksonFactory.getDefaultInstance(),new InputStreamReader(in)
        );
        List <String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                GoogleNetHttpTransport.newTrustedTransport(),JacksonFactory.getDefaultInstance(),
                clientSecrets,scopes)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File("tokens")))
                .setAccessType("offline")
                .build();
        Credential credential = new AuthorizationCodeInstalledApp(
                flow,new LocalServerReceiver())
                .authorize("user");

        return credential;
    }
    public static Sheets getSheetsService() throws IOException,GeneralSecurityException{
        Credential credential = authorize();
        return new Sheets.Builder(GoogleNetHttpTransport.newTrustedTransport(),
        JacksonFactory.getDefaultInstance(),credential)
                .setApplicationName(APLICATION_NAME)
                .build();
    }
    public static void main(String[] args) throws GeneralSecurityException, IOException {
        sheetsService = getSheetsService();
        List<String> myData = new LinkedList<>();
                myData.add("test");
                myData.add("test");
                myData.add("test");

            try {
                List<List<Object>> writeData = new ArrayList<>();
                for (String someData : myData) {
                    List<Object> dataRow = new ArrayList<>();
                    dataRow.add(someData);

                    writeData.add(dataRow);
                }

                com.google.api.services.sheets.v4.model.ValueRange vr = new ValueRange().setValues(writeData).setMajorDimension("ROWS");

                sheetsService.spreadsheets().values()
                        .update(SPREAD_SHEET_ID, "aaa", vr)
                        .setValueInputOption("RAW")
                        .execute();

            }
            catch (Exception e){}
    }
}
