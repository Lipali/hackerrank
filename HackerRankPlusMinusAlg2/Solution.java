import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

class Result {

    /*
     * Complete the 'plusMinus' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static void plusMinus(List<Integer> arr) {
        // Write your code here
        int counterPlus =0;
        int counterMinus =0;
        int counterZero =0;
        for (int i=0;i<arr.size();i++){

            if (arr.get(i).intValue()>0){
                counterPlus++;
            }
            if (arr.get(i).intValue()<0){
                counterMinus++;
            }
            if (arr.get(i).intValue()==0){
                counterZero++;
            }

        }
        double first = (Double.valueOf(counterPlus)/ arr.size()*10000)/10000;
        double secend = (Double.valueOf(counterMinus)/ arr.size()*10000)/10000;
        double third = (Double.valueOf(counterZero)/ arr.size()*10000)/10000;
        System.out.println(first);
        System.out.println(secend);
        System.out.println(third);

    }

}



public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        String[] arrTemp = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        List<Integer> arr = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrTemp[i]);
            arr.add(arrItem);
        }

        Result.plusMinus(arr);

        bufferedReader.close();
    }
}

