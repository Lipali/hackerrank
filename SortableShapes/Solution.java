import java.util.ArrayList;
import java.util.Collections;

public class Solution {
    public static void main(String[] args) {

        double side = 1.1234;
        double radius = 1.1234;
        double base = 5;
        double height = 2;

        ArrayList<Shape> shapes = new ArrayList<Shape>();
        shapes.add(new Square(side));
        shapes.add(new Circle(radius));
        shapes.add(new Triangle(base, height));

        for (Shape field : shapes) {
            System.out.println(field.areaCalculator());
        }
        Collections.sort(shapes);
        for (Shape field : shapes) {
            System.out.println(field.areaCalculator());
        }
    }
}
