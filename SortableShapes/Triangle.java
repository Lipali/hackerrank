public class Triangle extends Shape{
    double base;
    double height;

    public  Triangle (double base , double height){
        this.base=base;
        this.height=height;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double areaCalculator() {
        return base*height/2;
    }
    double size = areaCalculator();


    @Override
    public int compareTo(Shape o) {
        return (int) (this.areaCalculator()-o.areaCalculator())*-1;
    }
}
