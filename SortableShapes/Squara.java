class Square extends Shape{

    double side;
    public Square(double side){
        this.side=side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double areaCalculator() {
        return side*side;
    }
    double size = areaCalculator();

    @Override
    public int compareTo(Shape o) {
        return (int) (this.areaCalculator()-o.areaCalculator())*-1;
    }

}
