public class Circle extends Shape {
    double radius;
    double pi = Math.PI;
    public Circle(double radius){
        this.radius=radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    @Override
    public double areaCalculator() {
       return radius*radius*pi ;
    }
    double size = areaCalculator();

    @Override
    public int compareTo(Shape o) {
        return (int) (this.areaCalculator()-o.areaCalculator())*-1;
    }
}
